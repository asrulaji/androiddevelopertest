package com.asrul.androiddevelopertest.di

import com.asrul.androiddevelopertest.BuildConfig.ACCESS_TOKEN
import com.asrul.androiddevelopertest.BuildConfig.BASE_URL
import com.asrul.androiddevelopertest.BuildConfig.DEBUG
import com.asrul.androiddevelopertest.data.RepositoryImpl
import com.asrul.androiddevelopertest.data.network.ApiService
import com.asrul.androiddevelopertest.utils.AuthorizationInterceptor
import com.asrul.androiddevelopertest.repository.Repository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
        return OkHttpClient.Builder()
            .apply { if (DEBUG) addInterceptor(interceptor) }
            .addInterceptor(AuthorizationInterceptor(ACCESS_TOKEN))
            .build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(client: OkHttpClient): ApiService {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
            .create(ApiService::class.java)
    }

    @Provides
    @Singleton
    fun provideRepository(apiService: ApiService): Repository {
        return RepositoryImpl(apiService)
    }
}
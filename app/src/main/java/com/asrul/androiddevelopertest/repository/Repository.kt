package com.asrul.androiddevelopertest.repository

import androidx.paging.PagingData
import com.asrul.androiddevelopertest.data.network.Resource
import com.asrul.androiddevelopertest.data.network.response.GenresResponse
import com.asrul.androiddevelopertest.data.network.response.MovieDetailResponse
import com.asrul.androiddevelopertest.data.network.response.MoviesResponse
import com.asrul.androiddevelopertest.data.network.response.ReviewResponse
import kotlinx.coroutines.flow.Flow

interface Repository {
    fun getMovieGenres(): Flow<Resource<List<GenresResponse.Genre>>>
    fun getMovies(genre: String): Flow<PagingData<MoviesResponse.Movie>>
    fun getMovieDetail(id: Int): Flow<Resource<MovieDetailResponse>>
    fun getReview(movieId: Int): Flow<PagingData<ReviewResponse.Review>>
}
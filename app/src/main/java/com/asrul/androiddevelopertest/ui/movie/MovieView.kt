package com.asrul.androiddevelopertest.ui.movie

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import com.asrul.androiddevelopertest.data.network.response.MoviesResponse
import com.asrul.androiddevelopertest.databinding.FragmentMovieViewBinding
import com.asrul.androiddevelopertest.ui.cutomview.ErrorStateView
import com.asrul.androiddevelopertest.utils.Constants.DEFAULT_ERROR_MESSAGE
import com.asrul.androiddevelopertest.utils.setVisibility
import com.asrul.androiddevelopertest.utils.toVisible
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MovieView : Fragment() {

    private var _binding: FragmentMovieViewBinding? = null
    private val binding get() = _binding!!

    private val viewModel: MovieViewModel by viewModels()
    private val navArgs: MovieViewArgs by navArgs()
    private val movieAdapter: MovieAdapter by lazy {
        MovieAdapter(itemClickListener = object : MovieAdapter.OnClickListener {
            override fun onItemClicked(item: MoviesResponse.Movie) {
                findNavController().navigate(MovieViewDirections.actionMovieViewToMovieDetailView(item.id))
            }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMovieViewBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        observeMovies()
    }

    private fun initView() {
        movieAdapter.addLoadStateListener {
            // show a retry button outside the list when refresh hits an error
            binding.errorStateView.apply {
                setOnClickListener(object : ErrorStateView.ErrorStateClickListener {
                    override fun onActionButtonClicked() {
                        movieAdapter.retry()
                    }
                })
                errorMessage = DEFAULT_ERROR_MESSAGE
                setVisibility(it.refresh is LoadState.Error)
            }

            // show an empty state over the list when loading initially, before items are loaded
            val isFirstLoaded = it.refresh is LoadState.Loading && movieAdapter.itemCount == 0
            binding.shimmerContainer.setVisibility(isFirstLoaded)
        }

        binding.rvMovie.apply {
            val footerAdapter = LoadingStateAdapter { movieAdapter.retry() }
            val gridLayoutManager =
                GridLayoutManager(requireContext(), 2, GridLayoutManager.VERTICAL, false)

            gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int =
                    if (position == movieAdapter.itemCount && footerAdapter.itemCount > 0) 2 else 1
            }
            adapter = movieAdapter.withLoadStateFooter(footer = footerAdapter)
            layoutManager = gridLayoutManager
        }
    }

    private fun observeMovies() {
        viewModel.getMoviesByGenre(navArgs.genre)
            .observe(viewLifecycleOwner) {
                binding.rvMovie.toVisible()
                movieAdapter.submitData(lifecycle, it)
            }
    }
}
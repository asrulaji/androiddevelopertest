package com.asrul.androiddevelopertest.ui.moviedetail

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.transform.CircleCropTransformation
import com.asrul.androiddevelopertest.R
import com.asrul.androiddevelopertest.data.network.response.ReviewResponse
import com.asrul.androiddevelopertest.databinding.ItemUserReviewBinding
import com.asrul.androiddevelopertest.utils.appendWithImageUrl
import com.asrul.androiddevelopertest.utils.formatRatingDate
import com.asrul.androiddevelopertest.utils.setVisibility

class ReviewAdapter : PagingDataAdapter<ReviewResponse.Review, ReviewAdapter.ReviewViewHolder>(
    diffCallback
) {
    override fun onBindViewHolder(holder: ReviewViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReviewViewHolder {
        val binding =
            ItemUserReviewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ReviewViewHolder(binding)
    }

    inner class ReviewViewHolder(private val binding: ItemUserReviewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(review: ReviewResponse.Review) {
            with(binding) {
                ivAvatar.load(review.authorDetails.avatarPath.appendWithImageUrl()) {
                    crossfade(true)
                    placeholder(R.drawable.ic_launcher_foreground)
                    error(R.drawable.ic_launcher_foreground)
                    transformations(CircleCropTransformation())
                }
                tvName.text = review.authorDetails.name
                tvUsername.text = review.authorDetails.username
                tvDate.text = review.createdAt.formatRatingDate()
                tvReviewContent.text = review.content

                tvReviewContent.post {
                    tvShowMore.setVisibility(tvReviewContent.lineCount >= 3)
                }
                tvShowMore.setOnClickListener {
                    getItem(absoluteAdapterPosition)?.isExpanded =
                        !(getItem(absoluteAdapterPosition)?.isExpanded ?: false)

                    notifyItemChanged(absoluteAdapterPosition)
                }

                if (review.isExpanded) {
                    tvShowMore.text = root.context.getString(R.string.show_less)
                    tvReviewContent.maxLines = Int.MAX_VALUE
                } else {
                    tvShowMore.text = root.context.getString(R.string.show_more)
                    tvReviewContent.maxLines = 3
                }
            }
        }
    }

    companion object {
        val diffCallback = object : DiffUtil.ItemCallback<ReviewResponse.Review>() {
            override fun areItemsTheSame(
                oldItem: ReviewResponse.Review,
                newItem: ReviewResponse.Review
            ): Boolean = oldItem.id == newItem.id

            override fun areContentsTheSame(
                oldItem: ReviewResponse.Review,
                newItem: ReviewResponse.Review
            ): Boolean = oldItem == newItem
        }
    }
}
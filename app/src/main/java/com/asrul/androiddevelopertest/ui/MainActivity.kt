package com.asrul.androiddevelopertest.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.asrul.androiddevelopertest.R
import com.asrul.androiddevelopertest.databinding.ActivityMainBinding
import com.asrul.androiddevelopertest.utils.setVisibility
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val binding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }

    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        initView()
    }

    private fun initView() {
        navController = findNavController(R.id.fragmentContainerView)

        navController.addOnDestinationChangedListener { _, destination, _ ->
            binding.btnBack.setVisibility(destination.id != R.id.genresView)
        }
        binding.btnBack.setOnClickListener {
            navController.popBackStack()
        }
    }
}
package com.asrul.androiddevelopertest.ui.genre

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.asrul.androiddevelopertest.data.network.response.GenresResponse
import com.asrul.androiddevelopertest.databinding.ItemGenreBinding

class GenreAdapter(
    private val genres: List<GenresResponse.Genre>,
    val onItemClickListener: (GenresResponse.Genre) -> Unit
): RecyclerView.Adapter<GenreAdapter.GenreViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenreViewHolder {
        val binding = ItemGenreBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return GenreViewHolder(binding)
    }

    override fun onBindViewHolder(holder: GenreViewHolder, position: Int) {
        holder.bind(genres[position])
    }

    override fun getItemCount(): Int = genres.size

    inner class GenreViewHolder(private val binding: ItemGenreBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(item: GenresResponse.Genre) {
            binding.tvName.text = item.name
            binding.root.setOnClickListener {
                onItemClickListener(item)
            }
        }
    }
}
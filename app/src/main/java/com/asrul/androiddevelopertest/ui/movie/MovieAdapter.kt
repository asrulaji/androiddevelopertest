package com.asrul.androiddevelopertest.ui.movie

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.asrul.androiddevelopertest.R
import com.asrul.androiddevelopertest.data.network.response.MoviesResponse
import com.asrul.androiddevelopertest.databinding.ItemMovieBinding
import com.asrul.androiddevelopertest.utils.appendWithImageUrl

class MovieAdapter(
    private val itemClickListener: OnClickListener,
) : PagingDataAdapter<MoviesResponse.Movie, MovieAdapter.MovieViewHolder>(diffCallback) {

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val binding = ItemMovieBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MovieViewHolder(binding)
    }

    inner class MovieViewHolder(
        private val binding: ItemMovieBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: MoviesResponse.Movie) {
            with(binding) {
                tvTitle.text = item.title
                ivMovie.load(item.posterPath.appendWithImageUrl()) {
                    crossfade(true)
                    placeholder(R.drawable.ic_launcher_foreground)
                    error(R.drawable.ic_launcher_foreground)
                }
                root.setOnClickListener {
                    itemClickListener.onItemClicked(item)
                }
            }
        }
    }

    interface OnClickListener {
        fun onItemClicked(item: MoviesResponse.Movie)
    }

    companion object {
        val diffCallback = object : DiffUtil.ItemCallback<MoviesResponse.Movie>() {
            override fun areItemsTheSame(
                oldItem: MoviesResponse.Movie,
                newItem: MoviesResponse.Movie
            ): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: MoviesResponse.Movie,
                newItem: MoviesResponse.Movie
            ): Boolean {
                return oldItem == newItem
            }
        }
    }
}

package com.asrul.androiddevelopertest.ui.genre

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.asrul.androiddevelopertest.data.network.Resource
import com.asrul.androiddevelopertest.data.network.response.GenresResponse
import com.asrul.androiddevelopertest.repository.Repository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class GenreViewModel @Inject constructor(private val repository: Repository) : ViewModel() {

    private val _genre = MutableLiveData<Resource<List<GenresResponse.Genre>>>()
    val genre: LiveData<Resource<List<GenresResponse.Genre>>>
        get() = _genre

    fun getMovieGenres() {
        viewModelScope.launch {
            repository.getMovieGenres().collect {
                _genre.value = it
            }
        }
    }
}
package com.asrul.androiddevelopertest.ui.cutomview

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.asrul.androiddevelopertest.databinding.ErrorStateLayoutBinding
import com.asrul.androiddevelopertest.utils.setVisibility

class ErrorStateView: ConstraintLayout {

    private lateinit var binding: ErrorStateLayoutBinding
    private var onClickListener: ErrorStateClickListener? = null

    var errorMessage: String
        get() {
            return if (this::binding.isInitialized)
                binding.tvErrorMessage.text.toString()
            else ""
        }
        set(value) {
            if (this::binding.isInitialized)
                binding.tvErrorMessage.text = value
        }

    constructor(context: Context) : super(context) {
        initView()
    }
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initView()
    }
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        initView()
    }

    private fun initView() {
        binding = ErrorStateLayoutBinding.inflate(LayoutInflater.from(context), this, true)
        binding.btnAction.setOnClickListener {
            onClickListener?.onActionButtonClicked()
        }
    }

    fun setOnClickListener(listener: ErrorStateClickListener) {
        onClickListener = listener
    }

    fun setActionButtonVisibility(isVisible: Boolean) {
        binding.btnAction.setVisibility(isVisible)
    }

    fun setTitleVisibility(isVisible: Boolean) {
        binding.tvErrorTitle.setVisibility(isVisible)
    }

    interface ErrorStateClickListener {
        fun onActionButtonClicked()
    }
}
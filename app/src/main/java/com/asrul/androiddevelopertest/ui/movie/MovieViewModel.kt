package com.asrul.androiddevelopertest.ui.movie

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import com.asrul.androiddevelopertest.repository.Repository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MovieViewModel @Inject constructor(
    private val repository: Repository
) : ViewModel() {

    fun getMoviesByGenre(genre: String) = repository
        .getMovies(genre)
        .asLiveData()
        .cachedIn(viewModelScope)
}
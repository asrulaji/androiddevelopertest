package com.asrul.androiddevelopertest.ui.moviedetail

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import coil.load
import com.asrul.androiddevelopertest.R
import com.asrul.androiddevelopertest.data.network.Resource
import com.asrul.androiddevelopertest.databinding.DialogYoutubePlayerLayoutBinding
import com.asrul.androiddevelopertest.databinding.FragmentMovieDetailViewBinding
import com.asrul.androiddevelopertest.ui.movie.LoadingStateAdapter
import com.asrul.androiddevelopertest.utils.Constants.DEFAULT_ERROR_MESSAGE
import com.asrul.androiddevelopertest.utils.appendWithImageUrl
import com.asrul.androiddevelopertest.utils.minuteToTimeFormat
import com.asrul.androiddevelopertest.utils.setVisibility
import com.asrul.androiddevelopertest.utils.toGone
import com.asrul.androiddevelopertest.utils.toVisible
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.PlayerConstants
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MovieDetailView : Fragment() {

    private var _binding: FragmentMovieDetailViewBinding? = null
    private val binding get() = _binding!!

    private val viewModel: MovieDetailViewModel by viewModels()
    private val navArgs: MovieDetailViewArgs by navArgs()

    private val reviewAdapter: ReviewAdapter by lazy { ReviewAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMovieDetailViewBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getMovieDetail(navArgs.movieId)
        initView()
        observeMovieDetail()
        observeReview()
    }

    private fun initView() {
        binding.reviewErrorStateView.setActionButtonVisibility(false)
        binding.reviewErrorStateView.setTitleVisibility(false)
        reviewAdapter.addLoadStateListener {
            binding.reviewErrorStateView.apply {
                errorMessage = DEFAULT_ERROR_MESSAGE
                setVisibility(it.refresh is LoadState.Error)

                if (it.refresh is LoadState.NotLoading && reviewAdapter.itemCount == 0) {
                    errorMessage = requireContext().getString(R.string.no_review)
                    toVisible()
                }
            }
        }

        binding.rvReview.apply {
            val footerAdapter = LoadingStateAdapter { reviewAdapter.retry() }
            adapter = reviewAdapter.withLoadStateFooter(footer = footerAdapter)
            layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        }
    }

    private fun observeReview() {
        viewModel.getReview(navArgs.movieId)
            .observe(viewLifecycleOwner) {
                binding.rvReview.toVisible()
                reviewAdapter.submitData(lifecycle, it)
            }
    }

    private fun observeMovieDetail() {
        viewModel.movieDetail.observe(viewLifecycleOwner) { resource ->
            binding.apply {
                errorStateView.setVisibility(resource is Resource.Error)
                shimmerContainer.setVisibility(resource is Resource.Loading)
                detailContainer.setVisibility(resource is Resource.Success)
            }
            when (resource) {
                is Resource.Loading -> binding.shimmerContainer.startShimmer()
                is Resource.Error -> {
                    binding.shimmerContainer.stopShimmer()
                    binding.errorStateView.errorMessage = resource.message
                }

                is Resource.Success -> {
                    binding.shimmerContainer.stopShimmer()
                    binding.apply {
                        resource.data?.let {
                            ivMovie.load(it.posterPath.appendWithImageUrl())
                            tvTitle.text = it.title
                            tvTime.text = it.runtime.minuteToTimeFormat()
                            tvRating.text = String.format("%.1f/10", it.voteAverage)
                            tvOverviewValue.text = it.overview
                            tvYear.text = it.releaseDate
                            tvGenre.text =
                                it.genres.joinToString(" • ") { genre -> genre.name }
                        }
                    }
                }
            }

            val youtubeTrailerId = resource.data?.videos?.results?.firstOrNull { video ->
                video.site == "YouTube" && video.type == "Trailer"
            }?.key

            binding.btnPlay.setOnClickListener {
                showYoutubePlayerDialog(youtubeTrailerId)
            }
        }
    }

    private fun showYoutubePlayerDialog(videoId: String?) {
        val playerBinding = DialogYoutubePlayerLayoutBinding.inflate(layoutInflater, null, false)

        val dialog = Dialog(requireContext()).apply {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            setContentView(playerBinding.root)
            setCancelable(false)
        }

        playerBinding.apply {
            btnClose.setOnClickListener {
                youtubePlayer.release()
                dialog.dismiss()
            }
            videoId?.let {
                lifecycle.addObserver(playerBinding.youtubePlayer)
                youtubePlayer.addYouTubePlayerListener(object :
                    AbstractYouTubePlayerListener() {
                    override fun onReady(youTubePlayer: YouTubePlayer) {
                        super.onReady(youTubePlayer)
                        youTubePlayer.loadVideo(it, 0F)
                        youTubePlayer.play()
                        youtubePlayer.toVisible()
                        errorContainer.toGone()
                        progressBar.toGone()
                    }

                    override fun onError(
                        youTubePlayer: YouTubePlayer,
                        error: PlayerConstants.PlayerError
                    ) {
                        super.onError(youTubePlayer, error)
                        progressBar.toGone()
                        errorContainer.toVisible()
                        tvMessage.text = error.name
                    }
                })
            } ?: showErrorDialog(playerBinding)
        }

        dialog.show()

    }

    private fun showErrorDialog(dialogBinding: DialogYoutubePlayerLayoutBinding) {
        dialogBinding.apply {
            tvMessage.text = requireContext().getString(R.string.video_error_message)
            progressBar.toGone()
            youtubePlayer.toGone()
        }
    }
}
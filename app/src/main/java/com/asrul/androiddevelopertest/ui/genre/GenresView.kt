package com.asrul.androiddevelopertest.ui.genre

import android.animation.ObjectAnimator
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.asrul.androiddevelopertest.data.network.Resource
import com.asrul.androiddevelopertest.databinding.FragmentGenresViewBinding
import com.asrul.androiddevelopertest.ui.cutomview.ErrorStateView
import com.asrul.androiddevelopertest.utils.setVisibility
import com.asrul.androiddevelopertest.utils.toVisible
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class GenresView : Fragment() {

    private var _binding: FragmentGenresViewBinding? = null
    private val binding get() = _binding!!

    private val viewModel: GenreViewModel by viewModels()
    private var scrollState: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentGenresViewBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getMovieGenres()
        initView()
    }

    private fun initView() {
        binding.errorStateView.setOnClickListener(object : ErrorStateView.ErrorStateClickListener {
            override fun onActionButtonClicked() {
                viewModel.getMovieGenres()
            }
        })
        viewModel.genre.observe(viewLifecycleOwner) { resource ->
            binding.apply {
                rvGenre.setVisibility(resource is Resource.Success)
                shimmerContainer.setVisibility(resource is Resource.Loading)
                errorStateView.setVisibility(resource is Resource.Error)
            }
            when (resource) {
                is Resource.Loading -> binding.shimmerContainer.startShimmer()
                is Resource.Error -> {
                    binding.shimmerContainer.stopShimmer()
                    binding.errorStateView.errorMessage = resource.message
                }
                is Resource.Success -> {
                    binding.shimmerContainer.stopShimmer()
                    binding.rvGenre.apply {
                        layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
                        adapter = GenreAdapter(resource.data.orEmpty()) {
                            findNavController().navigate(GenresViewDirections.actionGenresViewToMovieView(it.id.toString()))
                        }
                    }

                    binding.btnScroll.toVisible()
                    binding.btnScroll.setOnClickListener {
                        if (scrollState) {
                            ObjectAnimator
                                .ofFloat(binding.icArrow, "rotation", 270f)
                                .start()
                            binding.rvGenre.smoothScrollToPosition(0)
                        } else {
                            ObjectAnimator
                                .ofFloat(binding.icArrow, "rotation", 90f)
                                .start()
                            binding.rvGenre.smoothScrollToPosition(resource.data?.size ?: 0)
                        }

                        scrollState = !scrollState
                    }
                }
            }
        }
    }
}
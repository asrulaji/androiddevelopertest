package com.asrul.androiddevelopertest.ui.moviedetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import com.asrul.androiddevelopertest.data.network.Resource
import com.asrul.androiddevelopertest.data.network.response.MovieDetailResponse
import com.asrul.androiddevelopertest.repository.Repository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MovieDetailViewModel @Inject constructor(
    private val repository: Repository
) : ViewModel() {

    private val _movieDetail = MutableLiveData<Resource<MovieDetailResponse>>()
    val movieDetail: LiveData<Resource<MovieDetailResponse>>
        get() = _movieDetail

    fun getMovieDetail(id: Int) {
        viewModelScope.launch {
            repository.getMovieDetail(id).collect {
                _movieDetail.value = it
            }
        }
    }

    fun getReview(id: Int) = repository
        .getReview(id)
        .asLiveData()
        .cachedIn(viewModelScope)
}
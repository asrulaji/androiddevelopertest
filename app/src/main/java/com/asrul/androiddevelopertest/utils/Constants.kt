package com.asrul.androiddevelopertest.utils

object Constants {
    const val DEFAULT_ERROR_MESSAGE = "An unexpected error occurred"
    const val NO_DATA_MESSAGE = "Data is empty"

    const val INITIAL_PAGE_INDEX = 1
}
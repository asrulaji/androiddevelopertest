package com.asrul.androiddevelopertest.utils

import android.view.View
import com.asrul.androiddevelopertest.BuildConfig.IMAGE_BASE_URL
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

fun View.toGone() {
    this.visibility = View.GONE
}
fun View.toVisible() {
    this.visibility = View.VISIBLE
}
fun View.setVisibility(isVisible: Boolean) {
    if (isVisible) this.visibility = View.VISIBLE
    else this.visibility = View.GONE
}

fun String?.appendWithImageUrl(): String {
    return "${IMAGE_BASE_URL}$this"
}
fun String?.formatRatingDate(): String {
    val originalFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault())
    val originalDate = this?.let { originalFormat.parse(it) } ?: Date()
    return SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.getDefault()).format(originalDate)
}

fun Int?.minuteToTimeFormat(): String {
    return "${this?.div(60)}h ${this?.mod(60)}m"
}
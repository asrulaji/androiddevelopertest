package com.asrul.androiddevelopertest.data.network.pagingsource

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.asrul.androiddevelopertest.data.network.ApiService
import com.asrul.androiddevelopertest.data.network.response.MoviesResponse
import com.asrul.androiddevelopertest.utils.Constants.INITIAL_PAGE_INDEX

class MoviePagingSource(
    private val apiService: ApiService,
    private val genre: String
) : PagingSource<Int, MoviesResponse.Movie>() {

    override fun getRefreshKey(state: PagingState<Int, MoviesResponse.Movie>): Int? {
        return state.anchorPosition?.let {
            val anchorPage = state.closestPageToPosition(it)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, MoviesResponse.Movie> {
        return try {
            val position = params.key ?: INITIAL_PAGE_INDEX
            val response = apiService.getMovies(position, genre)

            LoadResult.Page(
                data = response.results,
                prevKey = if (position == INITIAL_PAGE_INDEX) null else position - 1,
                nextKey = if (response.results.isEmpty()) null else position + 1
            )
        } catch (err: Exception) {
            return LoadResult.Error(err)
        }
    }
}
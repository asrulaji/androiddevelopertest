package com.asrul.androiddevelopertest.data.network.pagingsource

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.asrul.androiddevelopertest.data.network.ApiService
import com.asrul.androiddevelopertest.data.network.response.ReviewResponse
import com.asrul.androiddevelopertest.utils.Constants.INITIAL_PAGE_INDEX

class ReviewPagingSource(
    private val apiService: ApiService,
    private val movieId: Int
) : PagingSource<Int, ReviewResponse.Review>() {

    override fun getRefreshKey(state: PagingState<Int, ReviewResponse.Review>): Int? {
        return state.anchorPosition?.let {
            val anchorPage = state.closestPageToPosition(it)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, ReviewResponse.Review> {
        return try {
            val position = params.key ?: INITIAL_PAGE_INDEX
            val response = apiService.getReview(movieId, position)

            LoadResult.Page(
                data = response.results,
                prevKey = if (position == INITIAL_PAGE_INDEX) null else position - 1,
                nextKey = if (response.results.isEmpty()) null else position + 1
            )
        } catch (err: Exception) {
            return LoadResult.Error(err)
        }
    }
}
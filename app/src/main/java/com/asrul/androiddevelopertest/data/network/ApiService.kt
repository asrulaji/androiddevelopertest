package com.asrul.androiddevelopertest.data.network

import com.asrul.androiddevelopertest.data.network.response.GenresResponse
import com.asrul.androiddevelopertest.data.network.response.MovieDetailResponse
import com.asrul.androiddevelopertest.data.network.response.MoviesResponse
import com.asrul.androiddevelopertest.data.network.response.ReviewResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("genre/movie/list?language=en")
    suspend fun getMovieGenres(): GenresResponse

    @GET("discover/movie?include_adult=false&include_video=false&language=en-US&sort_by=popularity.desc")
    suspend fun getMovies(
        @Query("page") page: Int,
        @Query("with_genres") withGenres: String,
    ): MoviesResponse

    @GET("movie/{id}?append_to_response=videos&language=en-US")
    suspend fun getMovieDetail(
        @Path("id") id: Int
    ): MovieDetailResponse

    @GET("movie/{id}/reviews?language=en-US")
    suspend fun getReview(
        @Path("id") id: Int,
        @Query("page") page: Int
    ): ReviewResponse
}
package com.asrul.androiddevelopertest.data

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.asrul.androiddevelopertest.data.network.ApiService
import com.asrul.androiddevelopertest.data.network.pagingsource.MoviePagingSource
import com.asrul.androiddevelopertest.data.network.Resource
import com.asrul.androiddevelopertest.data.network.pagingsource.ReviewPagingSource
import com.asrul.androiddevelopertest.data.network.response.ErrorResponse
import com.asrul.androiddevelopertest.data.network.response.GenresResponse
import com.asrul.androiddevelopertest.data.network.response.MovieDetailResponse
import com.asrul.androiddevelopertest.data.network.response.MoviesResponse
import com.asrul.androiddevelopertest.data.network.response.ReviewResponse
import com.asrul.androiddevelopertest.repository.Repository
import com.asrul.androiddevelopertest.utils.Constants.DEFAULT_ERROR_MESSAGE
import com.asrul.androiddevelopertest.utils.Constants.NO_DATA_MESSAGE
import com.google.gson.Gson
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class RepositoryImpl @Inject constructor(private val apiService: ApiService) : Repository {
    override fun getMovieGenres(): Flow<Resource<List<GenresResponse.Genre>>> = flow {
        emit(Resource.Loading())

        try {
            val response = apiService.getMovieGenres()

            if (response.genres.isNotEmpty()) {
                emit(Resource.Success(response.genres))
            } else {
                emit(Resource.Error(NO_DATA_MESSAGE))
            }
        } catch (e: HttpException) {
            val err = Gson().fromJson(
                e.response()?.errorBody()?.string(),
                ErrorResponse::class.java
            )

            emit(Resource.Error(err.statusMessage.ifEmpty {
                e.localizedMessage ?: DEFAULT_ERROR_MESSAGE
            }))
        } catch (e: IOException) {
            emit(Resource.Error(e.localizedMessage ?: DEFAULT_ERROR_MESSAGE))
        }
    }

    override fun getMovies(genre: String): Flow<PagingData<MoviesResponse.Movie>> {
        return Pager(
            config = PagingConfig(pageSize = 20),
            pagingSourceFactory = {
                MoviePagingSource(apiService, genre)
            }
        ).flow
    }

    override fun getMovieDetail(id: Int): Flow<Resource<MovieDetailResponse>> = flow {
        emit(Resource.Loading())

        try {
            val response = apiService.getMovieDetail(id)
            emit(Resource.Success(response))
        } catch (e: HttpException) {
            val err = Gson().fromJson(
                e.response()?.errorBody()?.string(),
                ErrorResponse::class.java
            )

            emit(Resource.Error(err.statusMessage.ifEmpty {
                e.localizedMessage ?: DEFAULT_ERROR_MESSAGE
            }))
        } catch (e: IOException) {
            emit(Resource.Error(e.localizedMessage ?: DEFAULT_ERROR_MESSAGE))
        }
    }

    override fun getReview(movieId: Int): Flow<PagingData<ReviewResponse.Review>> {
        return Pager(
            config = PagingConfig(pageSize = 20),
            pagingSourceFactory = {
                ReviewPagingSource(apiService, movieId)
            }
        ).flow
    }
}